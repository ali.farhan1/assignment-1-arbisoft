// Import Statements

import React from "react";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

// Definition Statements

const Footer = () => (
    <div className={`footer`}>
        <Typography variant="body2">
            {"Copyright © "}
            <Link
                color="inherit"
                href="https://www.linkedin.com/in/farhan-kiyani/"
            >
                Farhan Kiyani
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
        </Typography>
    </div>
);

export default Footer;
