// page header with add new button with plus icon

import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

const NavBar = () => {
    return (
        <AppBar position="static" sx={{ marginBottom: 2 }}>
            <Toolbar>
                <Typography variant="h6" color="inherit">
                    Assignment 1
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

export default NavBar;
