import React, { useState, useEffect } from "react";
import axios from "axios";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import NavBar from "./components/NavBar";
import UserCard from "./components/UserCard";
import Progress from "./components/Progress";
import Footer from "./components/Footer";
import "./App.css";

function App() {
    const [userData, setUserData] = useState({});
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(function (response) {
                setUserData((prev) => {
                    return {
                        ...prev,
                        Users: response.data,
                    };
                });
                setLoading(false);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }, []);

    return (
        <React.Fragment>
            <CssBaseline />
            <Container>
                <NavBar />
                {isLoading ? <Progress /> : <UserCard users={userData.Users} />}
                <Footer />
            </Container>
        </React.Fragment>
    );
}

export default App;
